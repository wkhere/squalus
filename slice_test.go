package squalus

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func QuerySliceOfString(t *testing.T, db DB, _ *sql.DB) {
	var names []string
	if err := db.Query(
		context.Background(),
		"select [name] from [persons] order by [id]",
		nil,
		&names,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, []string{"Alice Abbott", "Bob Burton", "Clarissa Cooper", "Donald Dock"}, names, "Persons")

	// same test with a transaction
	names = nil
	tx, err := db.Begin(context.Background(), nil)
	assert.NoError(t, err)
	if err := tx.Query(
		context.Background(),
		"select [name] from [persons] order by [id]",
		nil,
		&names,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, []string{"Alice Abbott", "Bob Burton", "Clarissa Cooper", "Donald Dock"}, names, "Persons")
	assert.NoError(t, tx.Commit())
}

func QuerySliceOfStruct(t *testing.T, db DB, _ *sql.DB) {
	var people []Person
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] order by [id]",
		nil,
		&people,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, persons, people, "Persons")
}

func QuerySliceOfStructWithInClause(t *testing.T, db DB, _ *sql.DB) {
	expected := []Person{
		{
			ID:        1,
			Name:      "Alice Abbott",
			Height:    1.65,
			BirthDate: time.Date(1985, 7, 11, 0, 0, 0, 0, time.UTC),
		},
		{
			ID:        3,
			Name:      "Clarissa Cooper",
			Height:    1.68,
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
		},
		{
			ID:        4,
			Name:      "Donald Dock",
			Height:    1.71,
			BirthDate: time.Date(1954, 12, 4, 0, 0, 0, 0, time.UTC),
		},
	}

	var people []Person
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] where [id] in ({ids}) order by [id]",
		map[string]interface{}{"ids": []int{1, 3, 4}},
		&people,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, expected, people, "Persons")
}

type NameResult struct {
	First string
	Last  string
}

func (nr *NameResult) Scan(src interface{}) error {
	// some drivers return a string here, some return a []byte
	s := ""
	switch val := src.(type) {
	case string:
		s = val
	case []uint8:
		s = string(val)
	default:
		return fmt.Errorf("could not acquire field value (type %T) as string or []byte", src)
	}
	t := strings.Split(s, " ")
	if len(t) != 2 {
		return fmt.Errorf("format of %s is wrong: it should contain exactly one space", s)
	}
	nr.First, nr.Last = t[0], t[1]
	return nil
}

func QuerySliceOfStructsWithScan(t *testing.T, db DB, _ *sql.DB) {
	expected := []NameResult{
		{
			First: "Alice",
			Last:  "Abbott",
		},
		{
			First: "Bob",
			Last:  "Burton",
		},
		{
			First: "Clarissa",
			Last:  "Cooper",
		},
		{
			First: "Donald",
			Last:  "Dock",
		},
	}
	var names []NameResult
	if err := db.Query(
		context.Background(),
		"select [name] from [persons] order by [id]",
		nil,
		&names,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, expected, names, "Names")
}
