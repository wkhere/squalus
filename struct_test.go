package squalus

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func QueryStruct(t *testing.T, db DB, _ *sql.DB) {
	clarissaCooper := Person{
		ID:        3,
		Name:      "Clarissa Cooper",
		Height:    1.68,
		BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
	}
	var person Person
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person, "Person data")
}

func QueryStructEmbedded(t *testing.T, db DB, _ *sql.DB) {
	type Height struct {
		Height float64 `db:"height"`
	}
	type NameBirthHeight struct {
		Name      string    `db:"name"`
		BirthDate time.Time `db:"birth"`
		Height
	}
	type PersonEmbed struct {
		ID              int `db:"id"`
		NameBirthHeight `db:"name_birth_height"`
	}
	clarissaCooper := PersonEmbed{
		ID: 3,
		NameBirthHeight: NameBirthHeight{
			Name:      "Clarissa Cooper",
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
			Height: Height{
				Height: 1.68,
			},
		},
	}

	var person1 PersonEmbed
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person1,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person1, "Person data")

	var person2 PersonEmbed
	if err := db.Query(
		context.Background(),
		"select [name] as [name_birth_height.name], [id], [birth], [height] as [name_birth_height.height]"+
			" from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person2,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person2, "Person data")

	var person3 PersonEmbed
	if err := db.Query(
		context.Background(),
		"select [name] as [name_birth_height.name], [id], [birth], [height] as [name_birth_height.Height.height]"+
			" from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person3,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person3, "Person data")
}

func QueryStructComposed(t *testing.T, db DB, _ *sql.DB) {
	type Height struct {
		Height float64 `db:"height"`
	}
	type NameBirthHeight struct {
		Name      string    `db:"name"`
		BirthDate time.Time `db:"birth"`
		H         Height    `db:"hh"`
	}
	type PersonComposed struct {
		ID  int `db:"id"`
		NBH NameBirthHeight
	}
	clarissaCooper := PersonComposed{
		ID: 3,
		NBH: NameBirthHeight{
			Name:      "Clarissa Cooper",
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
			H: Height{
				Height: 1.68,
			},
		},
	}

	var person1 PersonComposed
	if err := db.Query(
		context.Background(),
		"select [name] as [NBH.name], [id], [birth] as [NBH.birth], [height] as [NBH.hh.height]"+
			" from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person1,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person1, "Person data")
}

func QueryStructFail(t *testing.T, db DB, _ *sql.DB) {
	var person Person

	err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 42},
		&person,
	)
	if err == nil {
		t.Fatal("Query should fail to read a nonexistent person")
	}
	assert.Equal(t, err, sql.ErrNoRows)

	err = db.Query(
		context.Background(),
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	)
	assert.Error(t, err, "Query to struct should fail because of a missing field")

	err = db.Query(
		context.Background(),
		"select [name], 'Beep' as [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	)
	assert.Error(t, err, "Query to struct should fail because of a field type mismatch")

	var people []Person
	err = db.Query(
		context.Background(),
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&people,
	)
	assert.Error(t, err, "Query to []struct should fail because of a missing field")

	peopleChan := make(chan Person)
	err = db.Query(
		context.Background(),
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		peopleChan,
	)
	assert.Error(t, err, "Query to chan struct should fail because of a missing field")

	err = db.Query(
		context.Background(),
		"select [name], [id], [birth], [height], 42 as [forty_two] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		func() {},
	)
	assert.Error(t, err, "Query to struct via callback should fail because of a missing field")

	err = db.Query(
		context.Background(),
		"select [name], 'Beep' as [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&people,
	)
	assert.Error(t, err, "Query to []struct should fail because of a field type mismatch")

	err = db.Query(
		context.Background(),
		"I see a red door",
		map[string]interface{}{"id": 3},
		&person,
	)
	assert.Error(t, err, "Query to struct should fail because of an invalid query")
}
