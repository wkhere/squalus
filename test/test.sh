#!/bin/sh

echo "Building project"
go build || exit 1

GOTESTARG="-p 1 -failfast -v -race -timeout 10m"
if [ "$1" = "--main" ]; then
    echo "Running gofmt"
    FMTDIFF=`gofmt -s -d *.go`
    if [ "$FMTDIFF" != "" ]
    then
        echo "Gofmt found error(s)"
        printf '%s\n' "$FMTDIFF"
        exit 1
    fi

    echo "Running goimports"
    IMPORTSDIFF=`goimports -d *.go`
    if [ "$IMPORTSDIFF" != "" ]
    then
        echo "Goimports found error(s)"
        printf '%s\n' "$IMPORTSDIFF"
        exit 1
    fi

    echo "Running go vet"
    go vet || exit 1

    echo "Running shadow"
    go vet -vettool=/go/bin/shadow || exit 1

    echo "Running revive"
    revive -config /go/test/revive.toml || exit 1

    echo "Running gocritic"
    gocritic check || exit 1

    echo "Running staticcheck"
    staticcheck || exit 1

    echo "Running errcheck"
    errcheck -asserts || exit 1

    echo "Running unconvert"
    unconvert || exit 1

    echo "Running misspell"
    misspell -error *.{go,yml,md} || exit 1

    echo "Running unit tests"
    go test $GOTESTARGS -coverprofile coverage.out
else
    go test $GOTESTARGS
fi
