package squalus

import (
	"context"
	"database/sql"
)

// Tx represents a Squalus connection for a transaction.
//
// Commit validates the transaction.
//
// Rollback cancels the transaction.
type Tx interface {
	SQLLink
	Commit() error
	Rollback() error
}

type txQuerier struct {
	tx  *sql.Tx
	drv Driver
}

func (tq txQuerier) driver() Driver {
	return tq.drv
}

func (tq txQuerier) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return tq.tx.ExecContext(ctx, query, args...)
}

func (tq txQuerier) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return tq.tx.QueryContext(ctx, query, args...)
}

func (tq txQuerier) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return tq.tx.QueryRowContext(ctx, query, args...)
}

// transaction represents a database transaction.
type transaction struct {
	drv Driver
	tx  *sql.Tx
}

// Commit commits the transaction.
func (tx *transaction) Commit() error {
	return tx.tx.Commit()
}

// Exec executes a query without returning any rows.
func (tx *transaction) Exec(ctx context.Context, query string, params map[string]interface{}) (sql.Result, error) {
	return internalExec(ctx, &txQuerier{drv: tx.drv, tx: tx.tx}, query, params)
}

// Query runs a query with parameters and returns the results.
func (tx *transaction) Query(
	ctx context.Context, query string, params map[string]interface{}, to interface{},
) error {
	return internalQuery(ctx, &txQuerier{drv: tx.drv, tx: tx.tx}, query, params, to)
}

// Rollback aborts the transaction.
func (tx *transaction) Rollback() error {
	return tx.tx.Rollback()
}
