package squalus

type postgresqlDriver struct{}

// NewPostgresqlDriver creates a `Driver` for postgresql databases.
// It is meant to be used with `NewDBWithDriver`
func NewPostgresqlDriver() Driver {
	return postgresqlDriver{}
}

func (d postgresqlDriver) AdaptQuery(query string, params map[string]interface{}) (string, []interface{}, error) {
	return adaptQueryNumbered(replaceSquareBracketsWithDoubleQuotes(query), params, "$")
}
