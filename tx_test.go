package squalus

import (
	"context"
	"database/sql"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TransactionCommit(t *testing.T, db DB, _ *sql.DB) {
	ctx := context.Background()
	tx, err := db.Begin(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	assert.NoError(t, err)
	_, err = tx.Exec(
		ctx,
		"insert into [persons]([id], [name], [height], [birth]) values({id}, {name}, {height}, {birth})",
		map[string]interface{}{
			"id":     5,
			"name":   "Étienne Étienne",
			"height": 1.77,
			"birth":  time.Date(1999, 9, 9, 0, 0, 0, 0, time.UTC),
		},
	)
	assert.NoError(t, err)

	if !strings.Contains(t.Name(), "mssql") { // MS SQL does not support this
		// before the transaction is committed, the person with ID 5 does not exist in the database.
		count, errCount := countPersonsWithID(db.Query, 5)
		assert.NoError(t, errCount)
		assert.Equal(t, 0, count)
	}

	// however, it exists in the scope of the transaction.
	count, err := countPersonsWithID(tx.Query, 5)
	assert.NoError(t, err)
	assert.Equal(t, 1, count)

	assert.NoError(t, tx.Commit())
	// after the transaction is committed, the person with ID 5 exists in the database.
	count, err = countPersonsWithID(db.Query, 5)
	assert.NoError(t, err)
	assert.Equal(t, 1, count)

	// cleanup
	_, err = db.Exec(ctx, "delete from [persons] where id={id}", map[string]interface{}{"id": 5})
	assert.NoError(t, err)
}

func TransactionRollback(t *testing.T, db DB, _ *sql.DB) {
	ctx := context.Background()
	tx, err := db.Begin(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	assert.NoError(t, err)
	_, err = tx.Exec(
		ctx,
		"insert into [persons]([id], [name], [height], [birth]) values({id}, {name}, {height}, {birth})",
		map[string]interface{}{
			"id":     6,
			"name":   "Françoise Fleurie",
			"height": 1.66,
			"birth":  time.Date(1998, 8, 8, 0, 0, 0, 0, time.UTC),
		},
	)
	assert.NoError(t, err)

	if !strings.Contains(t.Name(), "mssql") { // MS SQL does not support this
		// before the transaction is rolled back, the person with ID 6 does not exist in the database.
		count, errCount := countPersonsWithID(db.Query, 6)
		assert.NoError(t, errCount)
		assert.Equal(t, 0, count)
	}

	// however, it exists in the scope of the transaction.
	count, err := countPersonsWithID(tx.Query, 6)
	assert.NoError(t, err)
	assert.Equal(t, 1, count)

	assert.NoError(t, tx.Rollback())

	// after the transaction is rolled back, the person with ID 6 does not exist in the database.
	count, err = countPersonsWithID(db.Query, 6)
	assert.NoError(t, err)
	assert.Equal(t, 0, count)

	// cleanup
	_, err = db.Exec(ctx, "delete from [persons] where id={id}", map[string]interface{}{"id": 6})
	assert.NoError(t, err)
}
