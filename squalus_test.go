package squalus

import (
	"context"
	"database/sql"
	sqldriver "database/sql/driver"
	"errors"
	"os"
	"reflect"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type DummyTx struct{}

func (d DummyTx) Commit() error {
	return nil
}

func (d DummyTx) Rollback() error {
	return nil
}

type DummyConn struct{}

func (d DummyConn) Prepare(_ string) (sqldriver.Stmt, error) {
	return nil, nil
}

func (d DummyConn) Close() error {
	return nil
}

func (d DummyConn) Begin() (sqldriver.Tx, error) {
	return DummyTx{}, nil
}

type DummyDriver struct{}

func (d DummyDriver) Open(_ string) (sqldriver.Conn, error) {
	return DummyConn{}, nil
}

type Person struct {
	ID        int       `db:"id"`
	Name      string    `db:"name"`
	Height    float64   `db:"height"` // in meters
	BirthDate time.Time `db:"birth"`
}

var persons = []Person{
	{
		ID:        1,
		Name:      "Alice Abbott",
		Height:    1.65,
		BirthDate: time.Date(1985, 7, 11, 0, 0, 0, 0, time.UTC),
	},
	{
		ID:        2,
		Name:      "Bob Burton",
		Height:    1.59,
		BirthDate: time.Date(1977, 3, 1, 0, 0, 0, 0, time.UTC),
	},
	{
		ID:        3,
		Name:      "Clarissa Cooper",
		Height:    1.68,
		BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
	},
	{
		ID:        4,
		Name:      "Donald Dock",
		Height:    1.71,
		BirthDate: time.Date(1954, 12, 4, 0, 0, 0, 0, time.UTC),
	},
}

func TestSqualus(t *testing.T) {
	sql.Register("dummy", DummyDriver{})

	tests := []func(t *testing.T, sqDB DB, goDB *sql.DB){
		PopulateDatabase,
		QueryString,
		QueryFail,
		QueryMissingParam,
		QueryTime,
		QueryStruct,
		QueryStructEmbedded,
		QueryStructComposed,
		QueryStructFail,
		QueryStructConverter,
		QueryStructEmbeddedConverter,
		QueryStructComposedConverter,
		QuerySliceOfString,
		QuerySliceOfStruct,
		QuerySliceOfStructWithInClause,
		QuerySliceOfStructsWithScan,
		QueryChanOfStruct,
		QueryCallback,
		QueryCallbackError,
		ExecFail,
		NewDBFail,
		NewDBWithDriverOK,
		ByteSliceOperations,
		DeleteWithInClause,
		TransactionCommit,
		TransactionRollback,
	}

	mkTest := func(f func(t *testing.T, db DB, goDB *sql.DB), db DB, goDB *sql.DB) func(t *testing.T) {
		return func(t *testing.T) {
			f(t, db, goDB)
		}
	}

	type dualDB struct {
		sqDB DB
		goDB *sql.DB
	}
	dualDBs := make(map[string]dualDB)
	for _, f := range []func(t *testing.T) (string, DB, *sql.DB){
		ConnectToMysql,
		ConnectToSQLite,
		ConnectToPostgresql,
		ConnectToMssql,
	} {
		name, s, g := f(t)
		dualDBs[name] = dualDB{sqDB: s, goDB: g}
		defer func(db DB) {
			assert.NoError(t, db.Close())
		}(s)
	}
	defer func() {
		assert.NoError(t, os.Remove("./test.db"), "SQLite database file should have been deleted")
	}()

	for name, dual := range dualDBs {
		for _, test := range tests {
			spl := strings.Split(runtime.FuncForPC(reflect.ValueOf(test).Pointer()).Name(), ".")
			funcName := spl[len(spl)-1]
			t.Run(name+"-"+funcName, mkTest(test, dual.sqDB, dual.goDB))
		}
	}
}

func PopulateDatabase(t *testing.T, db DB, _ *sql.DB) {
	for _, p := range persons {
		result, err := db.Exec(
			context.Background(),
			"insert into [persons]([id], [name], [height], [birth]) values({id}, {name}, {height}, {birth})",
			map[string]interface{}{
				"id":     p.ID,
				"name":   p.Name,
				"height": p.Height,
				"birth":  p.BirthDate,
			},
		)
		if err != nil {
			t.Fatalf("Failed to Exec: %s", err)
		}
		rowsAffected, _ := result.RowsAffected()
		assert.Equal(t, int64(1), rowsAffected, "Rows affected")
	}
}

func QueryString(t *testing.T, db DB, _ *sql.DB) {
	var name string
	if err := db.Query(
		context.Background(),
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&name,
	); err != nil {
		t.Fatalf("Failed to read person name: %s", err)
	}
	assert.Equal(t, "Clarissa Cooper", name, "Person name")
}

func QueryFail(t *testing.T, db DB, _ *sql.DB) {
	var result string
	err := db.Query(
		context.Background(),
		"Be-Bop-A-Lula",
		map[string]interface{}{"id": 3},
		&result,
	)
	assert.Error(t, err, "Query (to string) should fail because of an invalid query")

	var results []string
	err = db.Query(
		context.Background(),
		"Be-Bop-A-Lula",
		map[string]interface{}{"id": 3},
		&results,
	)
	assert.Error(t, err, "Query (to slice) should fail because of an invalid query")

	var resultsInt []int
	err = db.Query(
		context.Background(),
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&resultsInt,
	)
	assert.Error(t, err, "Query (to slice) should fail because of an invalid result type")

	var resultsIntSlice [][]int
	err = db.Query(
		context.Background(),
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&resultsIntSlice,
	)
	assert.Error(t, err, "Query (to slice of slice of ints) should fail because of an invalid result type")

	resultsArray := [1]Person{}
	err = db.Query(
		context.Background(),
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&resultsArray,
	)
	assert.Error(t, err, "Query (to array) should fail because array is not a supported result type")

	var dummies [][1]string
	err = db.Query(
		context.Background(),
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&dummies,
	)
	assert.Error(t, err, "Query (to slice of arrays) should fail because a slice of arrays"+
		" is not a supported result type")
}

func QueryMissingParam(t *testing.T, db DB, _ *sql.DB) {
	var name string
	err := db.Query(
		context.Background(),
		"select [name] from [persons] where [id]={id}",
		map[string]interface{}{"kangaroo": 42},
		&name,
	)
	assert.Error(t, err, "Query should fail because of a missing parameter")
}

func QueryTime(t *testing.T, db DB, _ *sql.DB) {
	var birthDate time.Time
	if err := db.Query(
		context.Background(),
		"select [birth] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&birthDate,
	); err != nil {
		t.Fatalf("Failed to read person birth date: %s", err)
	}
	assert.Equal(t, time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC), birthDate, "Person birth date")
}

func QueryChanOfStruct(t *testing.T, db DB, _ *sql.DB) {
	ch := make(chan Person)
	var people []Person
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for p := range ch {
			people = append(people, p)
		}
		wg.Done()
	}()
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] order by [id]",
		nil,
		ch,
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	wg.Wait()
	assert.Equal(t, persons, people, "Persons")
}

func QueryCallback(t *testing.T, db DB, _ *sql.DB) {
	var people []Person
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] order by [id]",
		nil,
		func(name string, id int, birthDate time.Time, height float64) {
			people = append(people, Person{ID: id, Name: name, BirthDate: birthDate, Height: height})
		},
	); err != nil {
		t.Fatalf("Failed to read persons: %s", err)
	}
	assert.Equal(t, persons, people, "Persons")
}

func QueryCallbackError(t *testing.T, db DB, _ *sql.DB) {
	{
		// callback returns a result of type error with value nil
		var people []Person
		if err := db.Query(
			context.Background(),
			"select [name], [id], [birth], [height] from [persons] order by [id]",
			nil,
			func(name string, id int, birthDate time.Time, height float64) error {
				people = append(people, Person{ID: id, Name: name, BirthDate: birthDate, Height: height})
				return nil
			},
		); err != nil {
			t.Fatalf("Failed to read persons: %s", err)
		}
		assert.Equal(t, persons, people, "Persons")
	}
	{
		// Callback returns a non-nil error
		count := 0
		testError := errors.New("This is a test error")
		err := db.Query(
			context.Background(),
			"select [name], [id], [birth], [height] from [persons] order by [id]",
			nil,
			func(name string, id int, birthDate time.Time, height float64) error {
				count++
				if count == 3 {
					return testError
				}
				return nil
			},
		)
		assert.Equal(t, testError, err)
		assert.Equal(t, 3, count)
	}
	{
		// Callback returns a nil result but its type is not error
		var people []Person
		err := db.Query(
			context.Background(),
			"select [name], [id], [birth], [height] from [persons] order by [id]",
			nil,
			func(name string, id int, birthDate time.Time, height float64) *int {
				people = append(people, Person{ID: id, Name: name, BirthDate: birthDate, Height: height})
				return nil
			},
		)
		assert.Error(t, err)
	}
	{
		// Callback returns a non-nil result and its type is not error
		var people []Person
		err := db.Query(
			context.Background(),
			"select [name], [id], [birth], [height] from [persons] order by [id]",
			nil,
			func(name string, id int, birthDate time.Time, height float64) string {
				people = append(people, Person{ID: id, Name: name, BirthDate: birthDate, Height: height})
				return "hello"
			},
		)
		assert.Error(t, err)
	}
	{
		// Callback returns more than one result
		var people []Person
		err := db.Query(
			context.Background(),
			"select [name], [id], [birth], [height] from [persons] order by [id]",
			nil,
			func(name string, id int, birthDate time.Time, height float64) (int, string) {
				people = append(people, Person{ID: id, Name: name, BirthDate: birthDate, Height: height})
				return 42, "hello"
			},
		)
		assert.Error(t, err)
	}
}

func ExecFail(t *testing.T, db DB, _ *sql.DB) {
	result, err := db.Exec(context.Background(), "insert into [persons]([id]) values({id})", nil)
	assert.Error(t, err, "Exec should fail because of a missing parameter")
	assert.Nil(t, result)
}

type customDriver struct{}

func (customDriver) AdaptQuery(_ string, _ map[string]interface{}) (string, []interface{}, error) {
	return "", nil, nil
}

func NewDBFail(t *testing.T, _ DB, _ *sql.DB) {
	db1, err := sql.Open("dummy", "")
	if err != nil {
		t.Fatalf("Error: %s", err)
	}
	_, err = NewDB(db1)
	assert.Error(t, err, "Creating a Squalus DB should fail because the dummy driver is not supported")
}

func NewDBWithDriverOK(t *testing.T, _ DB, _ *sql.DB) {
	db1, err := sql.Open("dummy", "")
	if err != nil {
		t.Fatalf("Error: %s", err)
	}
	driver := customDriver{}
	customDB := NewDBWithDriver(db1, driver)
	assert.Equal(
		t,
		&connection{db: db1, driver: driver},
		customDB.(*connection),
		"Creating a Squalus DB with forced driver OK")
}

func ByteSliceOperations(t *testing.T, db DB, _ *sql.DB) {
	val := []byte{1, 2, 3, 4}
	params := map[string]interface{}{"val": val}
	result, err := db.Exec(context.Background(), "insert into [testbinary]([data]) values({val})", params)
	assert.NoError(t, err)
	rowsAffected, err := result.RowsAffected()
	assert.NoError(t, err)
	assert.Equal(t, int64(1), rowsAffected, "1 row should have been inserted")

	val2 := []byte{5, 6, 7, 8}
	params2 := map[string]interface{}{"val": val2}
	result, err = db.Exec(context.Background(), "insert into [testbinary]([data]) values({val})", params2)
	assert.NoError(t, err)
	rowsAffected, err = result.RowsAffected()
	assert.NoError(t, err)
	assert.Equal(t, int64(1), rowsAffected, "1 row should have been inserted")

	count := 0
	err = db.Query(context.Background(), "select count(*) from [testbinary] where [data]={val}", params, &count)
	assert.NoError(t, err)
	assert.Equal(t, count, 1, "1 row should have been found")

	var b []byte
	err = db.Query(context.Background(), "select [data] from [testbinary] where [data]={val}", params, &b)
	assert.NoError(t, err)
	assert.Equal(t, b, val, "Read value should be the same as written value")

	var bs [][]byte
	err = db.Query(
		context.Background(),
		"select [data] from [testbinary] where [data] in ({val}) order by [data]",
		map[string]interface{}{"val": [][]byte{val, val2}},
		&bs,
	)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(bs), "2 rows should have been read")
	assert.Equal(t, bs[0], val, "Read value should be the same as written value")
	assert.Equal(t, bs[1], val2, "Read value should be the same as written value")
}

func DeleteWithInClause(t *testing.T, db DB, goDB *sql.DB) {
	{
		// test empty IN clause
		_, errSQL := goDB.Exec("delete from persons where id in ()")
		_, errSqualus := db.Exec(
			context.Background(),
			"delete from persons where id in ({ids})",
			map[string]interface{}{"ids": []int64{}},
		)
		assert.Equal(t, errSQL, errSqualus, "Empty IN clause should not cause a Squalus error")
	}

	{
		// test IN clause containing exactly one element (particular case in Squalus)
		result, err := db.Exec(
			context.Background(),
			"delete from [persons] where [id] in ({ids})",
			map[string]interface{}{"ids": []int64{1}},
		)
		assert.NoError(t, err)
		ra, err := result.RowsAffected()
		assert.NoError(t, err, "Getting rows affected should succeed")
		assert.Equal(t, int64(1), ra, "There should be one row affected")
	}

	{
		// test IN clause containing several elements
		result, err := db.Exec(
			context.Background(),
			"delete from [persons] where [id] in ({ids})",
			map[string]interface{}{"ids": []int64{2, 3, 4}},
		)
		assert.NoError(t, err)
		ra, err := result.RowsAffected()
		assert.NoError(t, err, "Getting rows affected should succeed")
		assert.Equal(t, int64(3), ra, "There should be 3 rows affected")
	}
}

// countPersonsWithID (helper for the transaction tests) returns how many persons exist with the given ID.
func countPersonsWithID(
	query func(ctx context.Context, query string, params map[string]interface{}, to interface{}) error,
	id int,
) (int, error) {
	var count int
	err := query(
		context.Background(),
		"select count(*) from persons where id={id}",
		map[string]interface{}{"id": id},
		&count,
	)
	if err != nil {
		return 0, err
	}
	return count, nil
}
